﻿using System;
using System.Windows.Input;

namespace PointObs.Base
{
    public class RelayCommand: ICommand
    {
        Action executeMethod;
        Func<bool> canExecuteMethod;

        public RelayCommand(Action onExecuteMethod, Func<bool> onCanExecuteMethod)
        {
            executeMethod = onExecuteMethod;
            canExecuteMethod = onCanExecuteMethod;
        }

        #region ICommand Members

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return canExecuteMethod?.Invoke() ?? false;
        }

        public void Execute(object parameter)
        {
            executeMethod?.Invoke();
        }

        #endregion
    }
}
