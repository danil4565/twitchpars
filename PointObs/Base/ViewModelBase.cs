﻿using PropertyChanged;
using System.ComponentModel;

namespace PointObs.Base
{
    [AddINotifyPropertyChangedInterface]
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (o, e) => { };
        
    }
}
