﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;

namespace PointObs
{
    /// <summary>
    /// Interaction logic for ChatControl.xaml
    /// </summary>
    public partial class ChatControl : UserControl
    {
        public bool AutoScroll
        {
            get { return (bool)GetValue(AutoScrollProperty); }
            set { SetValue(AutoScrollProperty, value); }
        }

        public static readonly DependencyProperty AutoScrollProperty = DependencyProperty.Register(
                "AutoScroll",
                typeof(bool),
                typeof(ChatControl));
        public ChatControl()
        {
            InitializeComponent();
            (this.ChatList.Items as INotifyCollectionChanged).CollectionChanged += ChatControl_CollectionChanged; ; 
        }

        private void ChatControl_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (AutoScroll)
            {
                this.ChatScroller.ScrollToBottom();
            }
        }
    }
}
