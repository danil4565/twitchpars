﻿using CefSharp;
using CefSharp.OffScreen;
using System;
using System.Threading;


namespace PointObs
{
    public class ObserverSettings
    {
        public bool ObserveMessages { set; get; } = true;
        public bool ObservePointMessages { get; set; } = true;

        public bool IsDebugModeOn { get; set; } = false;
    }
    public class CefSharpPointObserver : IPointObservable
    {

        public event EventHandler<MessageReceivedEventArgs> OnMessageRecived;
        public event EventHandler<MessageReceivedEventArgs> OnPointMessageRecived;
        public event Action OnPointObsLoaded;
        public event EventHandler<SystemMessageReciverEventArgs> OnSystemMessageRecived;
        public event EventHandler<MessagePointRawardRecivedEventArgs> OnMessagePointRewardRecived;
        public event EventHandler<PointRawardRecivedEventArgs> OnPointRewardRecived;

        public ObserverSettings settings { get; set; }
        private ChromiumWebBrowser browser;
        public string CurrentChannel { get; private set; }
        public CefSharpPointObserver()
        {
            this.settings = new ObserverSettings();

        }
        public CefSharpPointObserver(ObserverSettings settings)
        {
            this.settings = settings;

        }
        public void Start()
        {
            CefSettings chromeSettings = new CefSettings();
            chromeSettings.WindowlessRenderingEnabled = true;

            Cef.Initialize(chromeSettings);
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            browser = new ChromiumWebBrowser();
            if (!browser.IsBrowserInitialized)
            {
                browser.RegisterAsyncJsObject("boundAsync", new CallbackObjectForJs(this));
            }
            while (!browser.IsBrowserInitialized)
            {
                Thread.Sleep(200);
            }
            if (settings.IsDebugModeOn)
            {
                browser.ShowDevTools();
            }
            OnPointObsLoaded?.Invoke();
            browser.FrameLoadEnd += (o, e) =>
            {
                if (e.Frame.IsMain)
                {
                    string script = "(async function() { await CefSharp.BindObjectAsync('boundAsync', 'bound');\n" +
                    $"var observeMessages = {settings.ObserveMessages.ToString().ToLower()};\n " +
                    $"var observePointMessages = {settings.ObservePointMessages.ToString().ToLower()};\n" +
                          System.IO.File.ReadAllText("content.js") + "})();";
                    browser.ExecuteScriptAsync(script);
                    OnSystemMessageRecived?.Invoke(this, new SystemMessageReciverEventArgs()
                    {
                        Channel = CurrentChannel,
                        Message = $"Loaded channel {CurrentChannel}",
                        TimeReached = DateTime.Now
                    });
                }
            };
        }
        public void LoadTwichCannel(string twitchChannelName)
        {
            CurrentChannel = twitchChannelName;
            browser.Load($"https://www.twitch.tv/popout/{ twitchChannelName.Replace(" ", "")}/chat");


        }
        public class CallbackObjectForJs
        {

            CefSharpPointObserver observer;
            public CallbackObjectForJs(CefSharpPointObserver observer)
            {
                this.observer = observer;
            }
            public void showMessage(string msg, string nickName) => observer.OnMessageRecived?.Invoke(this, new MessageReceivedEventArgs()
            {
                Channel = observer.CurrentChannel,
                Message = msg,
                NickName = nickName,
                TimeReached = DateTime.Now
            });
            public void showPointRaward(string message, string nickName, string pointPurchase, string pointCoast)
            {
                if (nickName != null)
                {
                    int coast = 600;
                    int.TryParse(pointCoast, out coast);
                    observer.OnMessagePointRewardRecived?.Invoke(this, new MessagePointRawardRecivedEventArgs()
                    {
                        PointCoast = coast,
                        Channel = observer.CurrentChannel,
                        Message = message,
                        NickName = nickName,
                        PointPurchaseName = pointPurchase,
                        TimeReached = DateTime.Now
                    });
                }
                else
                {
                    string[] splitedData = pointPurchase.Split(new char[] { ' ' }, 3);
                    int coast = 600;
                    int.TryParse(pointCoast, out coast);
                    observer.OnPointRewardRecived?.Invoke(this, new PointRawardRecivedEventArgs()
                    {
                        PointCoast = coast,
                        Channel = observer.CurrentChannel,
                        NickName = splitedData[0],
                        PointPurchaseName = splitedData[2],
                        TimeReached = DateTime.Now
                    });
                }
            }
            public void showPointMessage(string message, string nickName) => observer.OnPointMessageRecived?.Invoke(this,
                new MessageReceivedEventArgs()
                {
                    Channel = observer.CurrentChannel,
                    Message = message,
                    NickName = nickName,
                    TimeReached = DateTime.Now
                });
        }

    }
}
