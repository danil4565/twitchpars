﻿namespace PointObs
{
    public class ChatSystemMessage : IChatItem
    {
        public string Message { get; set; }
    }
}
