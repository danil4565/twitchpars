﻿namespace PointObs
{
    public interface IChatItem
    {
        string Message { get; set; }
    }
}
