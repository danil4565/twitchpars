﻿using System;

namespace PointObs
{

    public interface IPointObservable
    {
        event EventHandler<MessagePointRawardRecivedEventArgs> OnMessagePointRewardRecived;
        event EventHandler<PointRawardRecivedEventArgs> OnPointRewardRecived;
        event EventHandler<MessageReceivedEventArgs> OnMessageRecived;
        event EventHandler<MessageReceivedEventArgs> OnPointMessageRecived;
        event Action OnPointObsLoaded;
        event EventHandler<SystemMessageReciverEventArgs> OnSystemMessageRecived;
        void LoadTwichCannel(string twitchChannelName);
         
    }
    public class PointRawardRecivedEventArgs : EventArgs
    {
        public int PointCoast { get; set; }
        public string PointPurchaseName { get; set; }
        public string Channel { get; set; }
        public string NickName { get; set; }
        public DateTime TimeReached { get; set; }
    }

    public class MessagePointRawardRecivedEventArgs : PointRawardRecivedEventArgs
    {
        public string Message { get; set; }
    }

    public class SystemMessageReciverEventArgs: EventArgs
    {
        public string Channel { get; set; }
        public string Message { get; set; }
        public DateTime TimeReached { get; set; }
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        public string Channel { get; set; }
        public string NickName { get; set; }
        public string Message { get; set; }
        public DateTime TimeReached { get; set; }
    }
}