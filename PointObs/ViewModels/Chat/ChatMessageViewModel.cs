﻿using PointObs.Base;

namespace PointObs
{
    public class ChatMessageViewModel : ViewModelBase, IChatItem
    {
        public string NickName { get; set; }
        public string Message { get; set; }
    }
}
