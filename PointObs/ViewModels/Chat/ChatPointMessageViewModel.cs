﻿using PointObs.Base;

namespace PointObs
{
    public class ChatPointMessageViewModel : ViewModelBase, IChatItem
    {
        public string NickName { get; set; }
        public string Message { get; set; }
    }
}
