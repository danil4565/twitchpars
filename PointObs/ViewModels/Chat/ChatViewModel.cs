﻿using PointObs.Base;
using System;
using System.Collections.ObjectModel;

namespace PointObs
{
    public class ChatViewModel : ViewModelBase
    {
        public ObservableCollection<IChatItem> Messages { get; set; } = new ObservableCollection<IChatItem>();

        public bool ScrollChat { get; set; } = false;
        public void AddMessage(string Message, string NickName)
        {
            Messages.Add(new ChatMessageViewModel() { Message = Message, NickName = NickName });
        }
        public void AddSystemMessage(string Message)
        {
            Messages.Add(new ChatSystemMessage() { Message = Message });
        }
        public void AddPointMessage(string Message, string NickName)
        {
            Messages.Add(new ChatPointMessageViewModel() { Message = Message, NickName = NickName });
        }
    }
}
