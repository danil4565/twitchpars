﻿using PointObs.Base;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace PointObs.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Private Fields
        private IPointObservable PointObserver;
        private bool _isObserverLoaded = false;
        private Dispatcher Dispatcher = Application.Current.Dispatcher;
        #endregion

        #region Public Properties 
        public int NumberOfMessagesToInitiateClear { get; set; } = 200;
        public int NumberOfMessagesThatWillBeRemoved { get; set; } = 50;
        public string ChannelName { get; set; } = "Channel_Name";

        public bool Scroll { get; set; } = true;
        public bool IsNavigateButtonActive
        {
            get
            {
                var regEx = new Regex(@"^[a-zA-Z]{1}[a-zA-Z0-9_\s]*$");
                return regEx.IsMatch(ChannelName) && _isObserverLoaded;
            }
        }
        public ChatViewModel Chat { get; set; } = new ChatViewModel();
        #endregion

        public MainWindowViewModel()
        {

            PointObserver = new CefSharpPointObserver(new ObserverSettings()
            { ObserveMessages = true, ObservePointMessages = true, IsDebugModeOn = false }); ;
            PointObserver.OnMessageRecived += (o, e) => { Dispatcher.Invoke(() => { Chat.AddMessage(e.Message, e.NickName); }); };
            //PointObserver.OnMessagePointRewardRecived += (o, e) => {  Messages.Add($"{e.NickName} купил {e.PointPurchaseName} и сообщает {e.Message}\n"); };
            PointObserver.OnPointMessageRecived += (o, e) => { Dispatcher.Invoke(()=> Chat.AddPointMessage(e.Message, e.NickName)); };
            //PointObserver.OnPointRewardRecived += (o, e) => { Messages.Add($"{e.NickName} купил {e.PointPurchaseName}"); };
            PointObserver.OnPointObsLoaded += () => { _isObserverLoaded = true; };
            PointObserver.OnSystemMessageRecived += (o, e) => { Dispatcher.Invoke(() => Chat.AddSystemMessage($"========= Loaded channel: {e.Channel} ==========\n")); };

            (PointObserver as CefSharpPointObserver)?.Start();
            Chat.Messages.CollectionChanged += Messages_CollectionChanged;

            this.NavigateButtonCommand = new RelayCommand(OnExecuteNavigateButton, CanExecuteNavigateButton);
            this.ClearChatButtonCommand = new RelayCommand(OnExecuteClearChatButton, CanExecuteClearChatButton);
        }

        #region ClearButtonControls


        public ICommand ClearChatButtonCommand { get; set; }
        private void OnExecuteClearChatButton()
        {
            try 
            {
                Chat.Messages.RemoveRange(0, Chat.Messages.Count);
                Chat.AddSystemMessage("============ Cleared ==============");
            }
            catch
            {
                Chat.AddSystemMessage("Failed to clear chat!");

            }
        }
        private bool CanExecuteClearChatButton()
        {
            return true;
        }

        #endregion

        #region NavigateButtonControls

        public ICommand NavigateButtonCommand { get; set; }
        private void OnExecuteNavigateButton()
        {
            PointObserver.LoadTwichCannel(ChannelName.Replace(" ", ""));
        }
        private bool CanExecuteNavigateButton()
        {
            return IsNavigateButtonActive;
        }
        #endregion

        private void  Messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Chat.Messages.Count > NumberOfMessagesToInitiateClear)
                RemoveObsCollectionRange(Chat.Messages, 0, NumberOfMessagesThatWillBeRemoved);
        }

        private void RemoveObsCollectionRange<T>(ObservableCollection<T> obsCollection, int startIndex, int count)
        {
            Task.Run(() =>
            {
                Dispatcher.Invoke(() => obsCollection.RemoveRange(startIndex, count));
            });
        }
    }

    public static class ObservableCollectionExtention
    {
        public static void RemoveRange<T>(this ObservableCollection<T> collection, int startIndex, int count)
        {
            {
               for (int i = 0; i < count; i++)
               {
                    collection.RemoveAt(startIndex);
               }
            }
        }
    }
}
