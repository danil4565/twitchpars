
{
    var chat = document.getElementsByClassName('chat-scrollable-area__message-container tw-flex-grow-1 tw-pd-b-1')[0];
    var config = { childList: true };
    var callback = function (mutationsList) {
        for (var mutation of mutationsList) {

            if (mutation.addedNodes.length != 0) {
                switch (mutation.addedNodes[0].className) {
                    case "chat-line__message": 
                        if (observeMessages) {
                            var messageDiv = mutation.addedNodes[0].querySelector(".chat-line__no-background.tw-inline");
                            var nickName;
                            var nickNameTag = mutation.addedNodes[0].querySelector(".chat-line__username");
                            var message ="";
                            message = parseMessageFromChilds(message, messageDiv.childNodes);

                            if (nickNameTag == null){
                                nickNameTag = "null"
                            } else {
                                nickName = nickNameTag.innerText;
                            }
                            //console.log(message);
                            boundAsync.showMessage(message, nickName);
                        }
                        break;
                    case "tw-accent-region":
                        if (observePointMessages && 
                            mutation.addedNodes[0].querySelector(".channel-points-reward-line.tw-mg-y-05.tw-pd-r-2.tw-pd-y-05") != null && 
                            mutation.addedNodes[0].querySelector(".chat-line__message-body--highlighted") == null) {
                            var pointPurchase;
                            var pointCoast;
                            var nickName;
                            var message ="";
                            var pointPurchaseTag = mutation.addedNodes[0].querySelector(".tw-c-text-alt-2.tw-flex-wrap.tw-inline-flex").
                                childNodes[0];
                            var pointCoastTag = mutation.addedNodes[0].querySelector(".tw-c-text-alt-2.tw-flex-wrap.tw-inline-flex").
                                childNodes[2];
                            var nickNameTag = mutation.addedNodes[0].querySelector(".chat-author__display-name");   
                            var pointMessageTag = mutation.addedNodes[0].querySelector(".chat-line--inline.chat-line__message");
                            if (pointMessageTag != null){
                                var childs = pointMessageTag.children;
                                message = parseMessageFromChilds(message, childs);
                            }
                            // channel-points-reward-line tw-mg-y-05 tw-pd-r-2 tw-pd-y-05
                            // tw-mg-y-05 tw-pd-r-2 tw-pd-y-05 user-notice-line
                            pointCoast = pointCoastTag.textContent;
                            pointPurchase = pointPurchaseTag.textContent;
                            if (nickNameTag == null){
                                nickNameTag = "null"
                            } else {
                                nickName = nickNameTag.innerText;
                            }
                            boundAsync.showPointRaward(message, nickName, pointPurchase, pointCoast);
                        } else if (observePointMessages && 
                            mutation.addedNodes[0].querySelector(".channel-points-reward-line.tw-mg-y-05.tw-pd-r-2.tw-pd-y-05") != null && 
                            mutation.addedNodes[0].querySelector(".chat-line__message-body--highlighted") != null) {
                            
                            var nickName;
                            var message ="";
                            var nickNameTag = mutation.addedNodes[0].querySelector(".chat-author__display-name");   
                            var pointMessageTag = mutation.addedNodes[0].querySelector(".chat-line__message-body--highlighted");
                            if (pointMessageTag != null){
                                var childs = pointMessageTag.children;
                                message = parseMessageFromChilds(message, childs);
                                    
                                }
                            }
                                // channel-points-reward-line tw-mg-y-05 tw-pd-r-2 tw-pd-y-05
                                // tw-mg-y-05 tw-pd-r-2 tw-pd-y-05 user-notice-line

                            if (nickNameTag == null){
                                nickNameTag = "null"
                            } else {
                                nickName = nickNameTag.innerText;
                            }
                            boundAsync.showPointMessage(message, nickName);
                        }
                        break;
                }
            } 
        }

    
    
    var observer = new MutationObserver(callback);
    observer.observe(chat, config);

    const chatEmoteSelector = ".chat-image.chat-line__message--emote";

    function parseMessageFromChilds(message, childs){
        for(var i = 0; i < childs.length; i++){
            if (childs[i].className == "mention-fragment")
                message+= childs[i].innerText;
            else if (childs[i].className == "text-fragment")
                message+= childs[i].innerText;
            else if (childs[i].querySelector(chatEmoteSelector)!=null){
                console.log("success")
                message += (childs[i].querySelector(chatEmoteSelector).alt + " ");
            }
        };
        return message;
    };
} 

