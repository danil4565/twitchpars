
{
    var chat = document.getElementsByClassName('chat-list__list-container tw-flex-grow-1 tw-full-height tw-pd-b-1')[0];
    var config = { childList: true };
    var callback = function (mutationsList) {
        for (var mutation of mutationsList) {

            if (mutation.addedNodes.length != 0) {
                //console.log(mutation.addedNodes[0].className)
                switch (mutation.addedNodes[0].className) {
                    case "chat-line__message":
                        boundAsync.showMessage(mutation.addedNodes[0].innerText);
                        break;
                    case "":
                        boundAsync.showMessage(mutation.addedNodes[0].innerText);
                        break;
                    case "ffz-notice-line ffz--points-line tw-pd-l-1 tw-pd-y-05 tw-pd-r-2 ffz--points-highlight":
                        boundAsync.showMessage(mutation.addedNodes[0].innerText);
                        var pointAllertChildren = Array.from(mutation.addedNodes[0].childNodes);
                        for (var i = 0; i < pointAllertChildren.length; i++) {
                            if (pointAllertChildren[i].className == "tw-c-text-alt-2") {
                                var Children2 = Array.from(pointAllertChildren[i].childNodes);
                                for (var i = 0; i < Children2.length; i++) {
                                    if (Children2[i].className == "ffz--points-reward") {
                                        boundAsync.showMessage(Children2[i].innerText);
                                        break;
                                    }
                                }
                            }
                        }
                        boundAsync.showMessage(mutation.addedNodes[0].querySelectorAll('tw-c-text-alt-2'));

                        break;
                    
                        

                }
            }

        }
    }
    var observer = new MutationObserver(callback);
    observer.observe(chat, config);
} 